<?php

class MySql 
{
	private $host;
	private $username;
	private $password;
	private $database;
	private $prefix;
	private $query;
	private $connection;
	private $result;
	
	public function connect($connectionOptions)
	{
		$this->host = $connectionOptions['db_host'];
		$this->username = $connectionOptions['db_user'];
		$this->password = $connectionOptions['db_pass'];
		$this->database = $connectionOptions['db_name'];
		$this->connection = mysqli_connect($this->host, $this->username, $this->password);
		if(!$this->connection){
			throw new Exception("MySQL connect error: ".mysql_error()."<br />".$_SERVER['SCRIPT_NAME']);
		}
		if(!mysqli_select_db($this->connection, $this->database)){
			var_dump($connectionOptions);
			throw new Exception("MySQL select error: ".mysql_error()."<br />".$_SERVER['SCRIPT_NAME']);
		}
		return true;
	}
	
	public function disconnect()
	{
		if($this->connection){
			mysqli_close($this->connection);
		}
	}
	
	public function _query($sql)
	{
		//if($this->result){
			//mysqli_free_result($this->result);
		//}
		$this->result = mysqli_query($this->connection, $sql);
		return $this->result;
	}
	
	public function fetchArray()
	{
		return mysqli_fetch_assoc($this->result);
	}
	
	public function affectedRows(){
		$numRows = 0;
		$numRows = mysqli_affected_rows($this->connection);
		return $numRows;
	}
}
