<?php

class SqLite
{
	private $host;
	private $prefix;
	private $connection;
	private $result;
	
	public function connect($connectionOptions)
	{
		$this->host = $connectionOptions['db_host'];
		$this->connection = sqlite_open($this->host, 0666);
		if(!$this->connection){
			throw new Exception("SqLite error: " . sqlite_error_string(sqlite_last_error()) . "<br />" .$_SERVER['SCRIPT_NAME']);
		}
		return true;
	}
	
	public function disconnect()
	{
		if($this->connection){
			sqlite_close($this->connection);
		}
	}
	
	public function _query($sql)
	{
		$this->result = @sqlite_query($this->connection, $sql);
		return $this->result;
	}
	
	public function fetchArray()
	{
		return sqlite_fetch_array($this->result, SQLITE_ASSOC);
	}
	
	public function affectedRows()
	{
		$this->result ? sqlite_num_rows($this->result) : 0;
	}
}