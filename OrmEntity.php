<?php

class OrmEntity
{
	public $entityName;
	public $tableName;
	public $idField;
	public $fields = array();
	public $fieldLengths = array();
	
	public function __construct($entityName = '', $tableName = null)
	{
		$this->entityName = $entityName;
		if($tableName){
			$this->tableName = $tableName;
		}else{
			$this->tableName = $this->entityName; 
		}
	}
	
	public function addField($fieldName, $fieldType, $isId = false, $fieldLength = null)
	{
		if(isset($this->fields[$fieldName])){
			return false;
		}
		if($isId){
			$this->idField = $fieldName;
		}
		$this->fields[$fieldName] = $fieldType;
		$this->fieldLengths[$fieldName] = $fieldLength;
		return true;
	}
}
