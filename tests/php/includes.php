<?php

require_once('mockObjects.php');
require_once('tests/BaseTestOrmDb.php');
require_once('../../../OrmEntity.php');
require_once('../../../OrmEntityFactory.php');
require_once('../../../OrmEntityParser.php');
require_once('../../../OrmDb.php');
require_once('../../../MySql.php');
require_once('../../../MySqlOrm.php');
require_once('../../../SqLite.php');
require_once('../../../SqLiteOrm.php');