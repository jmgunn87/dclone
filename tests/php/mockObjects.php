<?php

/**
 * @Entity
 * @Table(name="TableIdAllColumns")
 */
class TableIdAllColumns
{
	/** @Id @Column(type="integer") */
	public $_id;
	/** @Column(type="smallint") */
	public $_smallint;
	/** @Column(type="bigint") */
	public $_bigint;
	/** @Column(type="string") */
	public $_string;
	/** @Column(type="text") */
	public $_text;
	/** @Column(type="array") */
	public $_array;
	/** @Column(type="object") */
	public $_object;
	
	public function __construct()
	{
		$this->_id = 0;
		$this->_smallint = 12;
		$this->_bigint = 145546;
		$this->_string = 'string';
		$this->_text = 'text';
		$this->_array = array(1, "string", array('key' => 'value'));
		$this->_object = new stdClass();
		$this->_object->name = 'stdObject';
	}
	
}
	