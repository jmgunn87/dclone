<?php

abstract class BaseTestOrmDb extends BaseUnitTest
{
	protected $db;
	
	public function testCreateTable()
	{
		return $this->assertTrue($this->db->createObjectTable('TableIdAllColumns'));
	}
	
	public function testCreateExisitingTable()
	{
		return $this->assertFalse($this->db->createObjectTable('TableIdAllColumns'));
	}
	
	public function testInsert()
	{
		return $this->assertTrue($this->db->insert(new TableIdAllColumns()));
	}
	
	public function testSelect()
	{
		return $this->assertTrue($this->db->select('TableIdAllColumns'));
	}
	
	public function testUpdateObject()
	{
		$selected = $this->db->select('TableIdAllColumns');
		$this->assertTrue($selected && count($selected) === 1);
		$this->db->insert($selected[0]);
		$selected = $this->db->select('TableIdAllColumns');
		return $this->assertTrue($selected && count($selected) === 1);
	}
	
	public function testDelete()
	{
		$selected = $this->db->select('TableIdAllColumns');
		$this->assertTrue($selected && count($selected) === 1);
		$this->db->delete($selected[0]);
	}
	
	public function testDropTable()
	{
		return $this->assertTrue($this->db->dropObjectTable('TableIdAllColumns'));
	}
}