<?php

class TestOrmDbSqLite extends BaseTestOrmDb
{
	public function __construct()
	{
		$dbConOpts = array(
			'db_driver' => 'pdo_sqlite',
			'db_host' => 'test.sqlite',
			'db_name' => 'test.sqlite',
			'db_user' => '',
			'db_pass' => ''
		);
		$this->db = new OrmDb();
		$this->db->connect($dbConOpts);
	}
	
	public function __destruct()
	{
		$this->db->disconnect();
		unlink('test.sqlite');
	}
}