<?php

class TestOrmDbMySql extends BaseTestOrmDb
{
	public function __construct()
	{
		$dbConOpts = array(
			'db_driver' => 'pdo_mysql',
			'db_host' => 'localhost',
			'db_name' => 'test',
			'db_user' => 'root',
			'db_pass' => ''
		);
		$this->db = new OrmDb();
		$this->db->connect($dbConOpts);
	}
	
	public function __destruct()
	{
		$this->db->disconnect();		
	}
}