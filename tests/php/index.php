<?php

require_once('../../../../unittester/BaseUnitTest.php');
require_once('../../../../unittester/UnitTestSuite.php');
require_once('includes.php');
require_once('../../../View.php');

$testSuite = new UnitTestSuite('orm');
foreach(glob('tests/Test*.php') as $test){
	$testSuite->AddUnitTest($test);
}

if($testSuite->runAllUnitTests() === false){
	var_dump($testSuite->unitTestResults);
	exit(1);	
}
