<?php

class MySqlOrm extends MySql
{	
	protected $entityFactory;
	
	public function __construct()
	{
		$this->entityFactory = new OrmEntityFactory();
	}

	public function createObjectTable($entity)
	{
		$sql = "CREATE TABLE IF NOT EXISTS $entity->tableName(";
		while($field = current($entity->fields)){
			$sql .= key($entity->fields) . ' ' . 
					$this->getDataTypeSql($field, $entity->fieldLengths[key($entity->fields)]);
			if($entity->idField == key($entity->fields)){
				$sql .= ' NOT NULL AUTO_INCREMENT PRIMARY KEY';
			}
			if(next($entity->fields)){
				$sql .= ',';
			}
		}
		return $this->_query($sql .= ");");
	}
	
	public function insert($entity, $object, $unique = false)
	{
		$sql = $unique ? "REPLACE INTO $entity->tableName VALUES(" : 
						 "INSERT INTO $entity->tableName VALUES(";

		$numberOfValidFields = count($entity->fields);
		$addedFields = 0;
		foreach($entity->fields as $fieldName => $fieldType){
			if($fieldName == $entity->idField){
				if($object->{$fieldName}){
					$sql .= $this->formatFieldValue($fieldType, $object->{$fieldName});
				}else{
					$sql .= 'null';
				}
			}else{
				$sql .= $this->formatFieldValue($fieldType, $object->{$fieldName});
			}
			++$addedFields;
			if($addedFields != $numberOfValidFields)
					$sql .= ',';
		}
		$this->_query($sql .= ');');
		$affectedRows = $this->affectedRows();
		if($affectedRows < 1){
			return $this->update($entity, $object);
		}else{
			return $affectedRows;
		}
	}
	
	private function update($entity, $object)
	{
		$sql = "UPDATE $entity->tableName SET ";
		$numberOfValidFields = count($entity->fields);
		$addedFields = 0;
		foreach($entity->fields as $fieldName => $fieldType){
			$sql .= $fieldName .' = '. $this->formatFieldValue($fieldType, $object->{$fieldName});
			++$addedFields;
			if($addedFields != $numberOfValidFields)
					$sql .= ',';
		}
		$this->_query("$sql WHERE $entity->idField = " . $object->{$entity->idField} . ';');
	}
	
	public function select($entity, $conditions = null, $limit = null, $sort = null, $sortDirection = null)
	{
		$sql = "SELECT * FROM $entity->tableName ";				
		if($conditions){
			$sql .= $this->parseConditions($conditions);
		}
		if($limit){
			$sql .= "LIMIT 0,$limit ";
		}
		if($sort){
			$sql .= "ORDER BY $sort $sortDirection ";
			//$query->orderBy($sort, $sortDirection ? $sortDirection : 'ASC');
		}
		return $this->instanciateResults($entity, $this->_query($sql));
	}
	
	private function instanciateResults($entity, $result)
	{
		if(!$result){
			return false;
		}
		$objectInstances = array();
		while($row = $this->fetchArray($result)){
			$objectInstances[] = $this->entityFactory->createInstance($entity, $row);
		}
		return $objectInstances;
	}
	
	public function delete($entity, $object)
	{
		$sql = "DELETE FROM $entity->tableName WHERE $entity->idField = $object->{$entity->idField}";
		return $this->_query($sql);
	}
	
	public function dropObjectTable($entity)
	{
		$sql = "DROP TABLE IF EXISTS $entity->tableName ;";
		return $this->_query($sql);
	}
	
	private function parseConditions($conditionsArray)
	{
		$sql = "WHERE ";
		while($value = current($conditionsArray)){
			$sql .= key($conditionsArray) . "='$value' ";
			if(next($conditionsArray)){
				$sql .= 'AND ';
			}
		}
		return $sql;
	}

	private function formatFieldValue($dataTypeId, $value)
	{
		switch($dataTypeId)
		{
			case ORM_TYPE_INT: 
			case ORM_TYPE_SMALLINT:
			case ORM_TYPE_BIGINT:
				return $value ? "$value" : 'NULL';
			case ORM_TYPE_DECIMAL:
			case ORM_TYPE_FLOAT:
				break;
			case ORM_TYPE_BOOL:
				return $value ? '1' : '0';
			case ORM_TYPE_DATE:
			case ORM_TYPE_TIME:
			case ORM_TYPE_DATETIME:
				break;
			case ORM_TYPE_STRING:
			case ORM_TYPE_TEXT:
				return "'$value'";
			case ORM_TYPE_OBJECT:
			case ORM_TYPE_ARRAY:
				return "'" . serialize($value) . "'";
			default:
				return 'NULL';
		}
		
	}
	private function getDataTypeSql($dataTypeId, $length)
	{
		switch($dataTypeId)
		{
			case ORM_TYPE_INT: 
				return $length ? "INT($length)" : 'INT';
			case ORM_TYPE_SMALLINT:
				return $length ? "SMALLINT($length)" : 'SMALLINT';
			case ORM_TYPE_BIGINT:
				return $length ? "BIGINT($length)" : 'BIGINT';
			case ORM_TYPE_DECIMAL: return 'DECIMAL';
			case ORM_TYPE_FLOAT: return 'FLOAT';
			case ORM_TYPE_BOOL: return 'BOOLEAN';
			case ORM_TYPE_DATE: return 'DATETIME';
			case ORM_TYPE_TIME: return 'TIME';
			case ORM_TYPE_DATETIME: return 'DATETIME/TIMESTAMP';
			case ORM_TYPE_STRING:
				return $length ? "VARCHAR($length)" : 'VARCHAR(255)';
			case ORM_TYPE_TEXT: return 'TEXT';
			case ORM_TYPE_OBJECT: return 'MEDIUMTEXT';
			case ORM_TYPE_ARRAY: return 'MEDIUMTEXT';
			default:
				return 'unknown';
		}
	}
}
