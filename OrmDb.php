<?php

class OrmDb
{
	protected $dbAdapter;
	protected $entityCache = array();
	protected $entityParser;

	public function __construct()
	{
		$this->entityParser = new OrmEntityParser();
	}
	
	public function connect($dbConOpts)
	{
		switch($dbConOpts['db_driver'])
		{
			case 'pdo_mysql':
				$this->dbAdapter = new MySqlOrm();
				break;
			case 'pdo_sqlite':
			default:
				$this->dbAdapter = new SqLiteOrm();
				break;
		}
		return $this->dbAdapter->connect($dbConOpts);
	}
	
	public function disconnect()
	{
		$this->dbAdapter->disconnect();
	}
	
	protected function getEntity($objectName)
	{
		if(isset($this->entityCache[$objectName])){
			return $this->entityCache[$objectName];
		}
		$entity = $this->entityParser->getClassEntity($objectName);
		if(!$entity->idField){
			throw new Exception('ORM: NO OBJECT ID!');
		}
		return $this->entityCache[$objectName] = $entity;
	}
	
	public function createObjectTable($objectName)
	{
		return $this->dbAdapter->createObjectTable($this->getEntity($objectName));
	}
	
	public function insert($object, $unique = false)
	{
		return $this->dbAdapter->insert($this->getEntity(get_class($object)), $object, $unique);
	}
	
	public function select($objectName, $conditions = null, $limit = null, $sort = null, $sortDirection = null)
	{
		return $this->dbAdapter->select($this->getEntity($objectName), $conditions, $limit, $sort, $sortDirection);
	}
	
	public function delete($object)
	{
		return $this->dbAdapter->createObjectTable($this->getEntity(get_class($object)), $object);
	}
	
	public function dropObjectTable($objectName)
	{
		return $this->dbAdapter->dropObjectTable($this->getEntity($objectName));
	}
}