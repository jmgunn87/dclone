<?php

class OrmEntityFactory
{
	public function createInstance(OrmEntity $entity, $values = array())
	{
		$fieldLength = strlen($entity->entityName);
		$objectString = 'O:' . $fieldLength . ':"' . $entity->entityName . '":';
		$fieldLength = count($entity->fields);
		$objectString .= $fieldLength . ':{';
		foreach($values as $key => $value){
			if(!isset($entity->fields[$key])){
				return false;
			}
			//enter the property name
			$fieldLength = strlen($key);
			$objectString .= 's:' . $fieldLength . ':"' . $key . '";';			
			//determine the property value
			$objectString .= $this->serializeValue($entity->fields[$key], $value);
		}
		return unserialize($objectString .= '}');
	}
	
	private function serializeValue($type, $value)
	{
		switch($type)
		{
			case ORM_TYPE_UNKNOWN:
				break;
			case ORM_TYPE_SMALLINT:
			case ORM_TYPE_BIGINT:
			case ORM_TYPE_INT:
				return 'i:' . $value . ';';
			case ORM_TYPE_DECIMAL:
				break;
			case ORM_TYPE_FLOAT:
				break;
			case ORM_TYPE_BOOL:
				return $value ? 'b:1;' : 'b:0;'; 
			case ORM_TYPE_DATE:
			case ORM_TYPE_TIME:
			case ORM_TYPE_DATETIME:
				return 'N;';
			case ORM_TYPE_STRING:
			case ORM_TYPE_TEXT:
				$valueLength = strlen($value);
				if($valueLength == 0){
					return 'N;';
				}
				return 's:' . $valueLength . ':"' . $value . '";';
			case ORM_TYPE_OBJECT:
			case ORM_TYPE_ARRAY:
				return $value;
			default:
				return 'N;';
		}
	}
}