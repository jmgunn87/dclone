<?php

define('ORM_ENTITY', 1);
define('ORM_TABLE', 2);
define('ORM_ID', 3);
define('ORM_COLUMN', 4);

define('ORM_TYPE_UNKNOWN', -1);
define('ORM_TYPE_INT', 2);
define('ORM_TYPE_SMALLINT', 3);
define('ORM_TYPE_BIGINT', 4);
define('ORM_TYPE_DECIMAL', 5);
define('ORM_TYPE_FLOAT', 6);
define('ORM_TYPE_BOOL', 7);
define('ORM_TYPE_DATE', 8);
define('ORM_TYPE_TIME', 9);
define('ORM_TYPE_DATETIME', 10);
define('ORM_TYPE_STRING', 11);
define('ORM_TYPE_TEXT', 12);
define('ORM_TYPE_OBJECT', 13);
define('ORM_TYPE_ARRAY', 14);

/*
    string: Type that maps an SQL VARCHAR to a PHP string.
    integer: Type that maps an SQL INT to a PHP integer.
    smallint: Type that maps a database SMALLINT to a PHP integer.
    bigint: Type that maps a database BIGINT to a PHP string.
    boolean: Type that maps an SQL boolean to a PHP boolean.
    decimal: Type that maps an SQL DECIMAL to a PHP double.
    date: Type that maps an SQL DATETIME to a PHP DateTime object.
    time: Type that maps an SQL TIME to a PHP DateTime object.
    datetime: Type that maps an SQL DATETIME/TIMESTAMP to a PHP DateTime object.
    text: Type that maps an SQL CLOB to a PHP string.
    object: Type that maps a SQL CLOB to a PHP object using serialize() and unserialize()
    array: Type that maps a SQL CLOB to a PHP object using serialize() and unserialize()
    float: Type that maps a SQL Float (Double Precision) to a PHP double. IMPORTANT: Works only with locale settings that use decimal points as separator.

 */

class OrmEntityParser
{
	public function getClassEntity($className)
	{
		$entity = new OrmEntity($className);
		$class = new ReflectionClass($className);
		$this->parseClassDocBlock($entity, $class->getDocComment());
		foreach($class->getProperties() as $property){
			if($property->getDocComment()){
				$this->parsePropertyDocBlock($entity, $property->getName(), $property->getDocComment());
			}
			
		}
		return $entity;
	}
	
	private function parseClassDocBlock($entity, $docBlock)
	{
		$tags = explode('@', $docBlock);
		foreach($tags as $tag){
			$tag = str_replace('"', '', $tag);
			switch($this->getCommand($tag))
			{
				case ORM_ENTITY:
					break;
				case ORM_TABLE:
					$tagOpts = explode('(', $tag);
					if(count($tagOpts) > 1){
						$tagOpts = explode(')', $tagOpts[1]);
						$tagOpts = $tagOpts[0];
					}else{
						$tagOpts = null;
					}
					if($tagOpts){
						$attributes = $this->getAttributes($tagOpts);
						foreach($attributes as $attrName => $attrValue){
							switch($attrName)
							{
								case 'name':
									$entity->tableName = $attrValue;
									break;
								default:
									break;
							}
						}
					}
					break;
				default:
					break;
			}
		}
	}
	
	private function parsePropertyDocBlock($entity, $propertyName, $docBlock)
	{
		$isId = false;
		$tags = explode('@', $docBlock);
		foreach($tags as $tag){
			$tag = str_replace('"', '', $tag);
			switch($this->getCommand($tag))
			{
				case ORM_ID:
					$isId = true;
					break;
				case ORM_COLUMN:
					$tagOpts = explode('(', $tag);
					if(count($tagOpts) > 1){
						$tagOpts = explode(')', $tagOpts[1]);
						$tagOpts = $tagOpts[0];
					}else{
						$tagOpts = ' ';
					}
					$fieldName = $propertyName;
					$fieldType = '';
					$fieldLength = '';
					$attributes = $this->getAttributes($tagOpts);
					foreach($attributes as $attrName => $attrValue){
						switch($attrName)
						{
							case 'name':
								$fieldName = $attrValue;
								break;
							case 'type':
								$fieldType = $this->convertDataTypeString($attrValue);
								break;
							case 'length':
								$fieldLength = $attrValue;
								break;
							default:
								break;
						}
					}
					$entity->addField($fieldName, $fieldType, $isId, $fieldLength);
					break;
				default:
					break;
			}
		}
	}

	private function getCommand($tag)
	{
		if(strstr($tag,'Entity')){
			return ORM_ENTITY;
		}
		if(strstr($tag,'Table')){
			return ORM_TABLE;
		}
		if(strstr($tag,'Id')){
			return ORM_ID;
		}
		if(strstr($tag,'Column')){
			return ORM_COLUMN;
		}
	}
	
	private function getAttributes($tag)
	{
		$parsedAttributes = array();
		$attributes = explode(',', $tag);
		foreach($attributes as $attribute){
			$attribute = explode('=', $attribute);
			$attributeName = isset($attribute[0]) ? $attribute[0] : '';
			$attributeValue = isset($attribute[1]) ? $attribute[1] : '';
			$parsedAttributes[$attributeName] = $attributeValue;
		}
		return $parsedAttributes;
	}
	
	private function convertDataTypeString($typeString)
	{
		switch($typeString)
		{
			case 'string':
				return ORM_TYPE_STRING;
			case 'integer':
				return ORM_TYPE_INT;
			case 'smallint':
				return ORM_TYPE_SMALLINT;
			case 'bigint':
				return ORM_TYPE_BIGINT;
			case 'boolean':
				return ORM_TYPE_BOOL;
			case 'decimal':
				return ORM_TYPE_DECIMAL;
			case 'date':
				return ORM_TYPE_DATE;
			case 'time':
				return ORM_TYPE_TIME;
			case 'datetime':
				return ORM_TYPE_DATETIME;
			case 'text':
				return ORM_TYPE_TEXT;
			case 'array':
				return ORM_TYPE_ARRAY;
			case 'object':
				return ORM_TYPE_OBJECT;	
			default:
				return ORM_TYPE_UNKNOWN;
		}
	}
}